/**
 * input:
 * so = 12 
 * 
 * todo:
 * soHangDonVi = so % 10
 * soHangChuc = so / 10
 * tong = soHangDonVi + soHangChuc
 * 
 * output:
 * tong
 */
 var so = 12;
 var soHangDonVi = so % 10;
 var soHangChuc = Math.floor(so / 10);
 var tong = soHangDonVi + soHangChuc;
console.log("Tong la: " + tong);
