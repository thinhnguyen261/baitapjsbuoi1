/**
 * input:
 * dai = 5 
 * rong = 3
 * 
 * todo:
 * dienTich =  dai * rong
 * chuVi =  (dai + rong) * 2
 * 
 * output:
 * dienTich
 * chuVi
 */
 var dai = 5;
 var rong = 3;
 var dienTich = dai * rong;
 var chuVi = (dai + rong) * 2;
console.log("Dien tich la: " + dienTich);
console.log("Chu Vi la: " + chuVi);
