/**
 * input:
 * giaTri1 = 1;
 * giaTri2 = 2;
 * giaTri3 = 3;
 * giaTri4 = 4;
 * giaTri5 = 5;
 * 
 * todo:
 * giaTriTrungBinh = (giaTri1 + giaTri2 + giaTri3 + giaTri4 + giaTri5) / 5
 * 
 * output:
 * giaTriTrungBinh
 */
 var giaTri1 = 1;
 var giaTri2 = 2;
 var giaTri3 = 3;
 var giaTri4 = 4;
 var giaTri5 = 5;
 var giaTriTrungBinh = (giaTri1 + giaTri2 + giaTri3 + giaTri4 + giaTri5) / 5;
 console.log("Gia tri trung binh la: " + giaTriTrungBinh);